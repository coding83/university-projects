clc;
%Euler_plotter(5, 100, 10);
%Euler_error(5, 10);
y_Runge = Runge_Kutta_error(5, 10);
y_Euler = Euler_error(5, 10, 500)
N = 1:500;
plot(N, log10(y_Euler), 'g');
hold on;
plot(N, log10(y_Runge), 'b');
xlabel('N, ���������� ���������');
ylabel('log_{10}\epsilon');
legend('����������� ������ �����-�����', '����������� ������ ������');

function eps = Euler_error(a, T, K)
N = linspace(1, K, K);
for i=1:K
h=T/N(i) ; 
x=linspace(0,T,N(i));
f =@(x) exp(-a*x);
y2(1) = 1;
y1(1) = -a;
for j=2:i 
    y2(j) = y2(j-1)+h*y1(j-1); 
    y1(j) = y1(j-1)+h*a^2*y2(j-1); 
end
eps(N(i)) = abs(y2(N(i))-f(T)); 
end
plot(N,log10(eps))
hold on;
%epsilon2= zeros(length(N));
%epsilon2 = epsilon2 - 3;
%plot(N, epsilon2, 'g'); 
%legend('����������� ������ ������', '10^{-3}');
end


function Euler_plotter(a, N, T)
h = T/N;
x = linspace(0, T, N);
y_1 = zeros(N);
y_2 = zeros(N);
y_1(1) = -a; %������ ���� �� ����������� y
y_2(1) = 1;
for i = 2:length(x)
    y_2(i) = y_2(i-1) + h*y_1(i-1);
    y_1(i) = y_1(i-1) + h*a*a*y_2(i-1);
end;
f = @(t) exp(-a*t);
t = linspace(0, T, 1000);
plot(t, f(t));
hold on;
plot(x, y_2);
%legend('������������� �������', '����� ������');
end

function eps = Runge_Kutta_error(a, T)
f = @(t) exp(-a.*t); 
A = [0, 1; 
    a^2, 0];
N_arr = 1:500;
eps = zeros(1, 500);
for N = 1:500
    h = T/N;
    x = linspace(0, T, N);
    y = zeros(2, N);
    y(:,1) = [1;-a];
    for i = 2:N
        y(:,i) = y(:,(i-1)) + h*A*(y(:,(i-1)) + (h/2)*A*y(:,(i-1))); 
    end
    eps(N) = abs(y(1,N) - f(T));
end
%plot(N_arr, log10(eps));
%xlabel('N');
%ylabel('log_{10}(\epsilon)');
%legend('����������� ������ �����-�����, � = 10');
end



